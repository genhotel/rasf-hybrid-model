# Metabolic Reprogramming in Rheumatoid Arthritis Synovial Fibroblasts: a Hybrid Modeling Approach

## Scope

This repository summarizes all the codes and data used to generate the results presented in [Metabolic Reprogramming in Rheumatoid Arthritis Synovial Fibroblasts: a Hybrid Modeling Approach, Aghakhani et al. (2022)](https://doi.org/10.1371/journal.pcbi.1010408). 
Please refer to the latter for all information concerning the methods used.


## Description

This repository includes:

1.  RASF-model's statistical analysis:

- Cellular-specificity assessment;
- Annotation score calculation.

2. Framework for hybrid modeling:

<div align="center">
<img src="Framework_for_hybrid_modeling/workflow.png" width="560" height="570">
</div>

3. Regulatory initial condition’s knock-out/knock-in simulations. 

## Contributors

- Sahar Aghakhani, [sahar.aghakhani@univ-evry.fr](sahar.aghakhani@univ-evry.fr);
- Sylvain Soliman, [sylvain.soliman@inria.fr](sylvain.soliman@inria.fr);
- Anna Niarakis, [anna.niaraki@univ-evry.fr](anna.niaraki@univ-evry.fr).
